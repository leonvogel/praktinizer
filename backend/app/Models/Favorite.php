<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Models\Posting;

class Favorite extends Model
{
    public function Posting()
    {
        return $this->belongsTo(Posting::class);
    }
}
