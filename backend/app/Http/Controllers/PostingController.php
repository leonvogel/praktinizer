<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Posting;
use Exception;

class PostingController extends Controller {

    public function list() {
        return Posting::all();
    }

    public function add(Request $request) {
        $this->validate($request, [
            'company' => 'required|max:255',
            'title' => 'required|max:255',
            'description' => 'required',
            'language' => 'required',
        ]);

        try {
            $posting = new Posting;
            $posting->company = $request->input('company');
            $posting->title = $request->input('title');
            $posting->description = $request->input('description');
            $posting->language = $request->input('language');

            $posting->save();

            return response()->json(['posting' => $posting, 'message' => 'CREATED'], 201);
        } catch(Exception $e) {
            return response()->json(['message' => 'Failed to add Posting!'], 409);
        }
    }
}