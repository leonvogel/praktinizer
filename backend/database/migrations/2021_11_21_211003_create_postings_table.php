<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostingsTable extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postings', function (Blueprint $table) {
            $table->id();
            $table->string('company')->required();
            $table->string('title')->required();
            #$table->string('location');
            $table->text('description')->required();
            $table->set('language', ['Deutsch', 'Englisch'])->required();
            #$table->string('flyer_file_path');
            #$table->date('start_date');
            #$table->date('end_date');
            #$table->boolean('is_active')->default(true);
            #$table->string('type');
            #$table->string('work_amount');
            #$table->float('pay')->default(0);
            #$table->boolean('final_thesis')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postings');
    }
}
