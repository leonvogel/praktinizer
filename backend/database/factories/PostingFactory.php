<?php

namespace Database\Factories;

use App\Models\Posting;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Posting::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => $this->faker->numberBetween(1, 9000),
            'description' => $this->faker->realText(50, 2),
            'company' => $this->faker->company(),
            'title' => $this->faker->realText(30, 2),
            'language' => 'Deutsch',
        ];
    }
}
