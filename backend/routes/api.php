<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/*
    Routes that don't need any authentification
*/
$router->group(['prefix' => 'api'], function () use ($router) {
    // Matches "/api/register
    $router->post('register', 'AuthController@register');
  
    // Matches "/api/login
    $router->post('login', 'AuthController@login');

});

/*
    Routes that need users to be logged in
*/
$router->group(['prefix' => 'api', 'middleware' => 'auth'], function () use ($router) {
    // Matches "/api/postings
    $router->get('postings', 'PostingController@list');    
});

/*
    Routes that need users to be logged in and admin role
*/
$router->group(['prefix' => 'api', 'middleware' => 'authAdmin'], function () use ($router) {
    // Matches "/api/postings
    $router->post('postings', 'PostingController@add');    
});


$router->get('/', function () use ($router) {
      $users = DB::table('users')->get();
      return $users;
});
