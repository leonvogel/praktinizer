# Setup
## In Backend:

### Lumen environment anpassen

`.env.example` zu `.env` umbenennen


**Datenbankverbindung in `.env` einrichten**
    
    DB_CONNECTION=mysql
    DB_HOST=db
    DB_PORT=3306
    DB_DATABASE=praktinizer
    DB_USERNAME=praktinizer
    DB_PASSWORD=secret
    MYSQL_ROOT_PASSWORD=root

`DB_DATABASE`, `DB_USERNAME`, `DB_PASSWORD` und `MYSQL_DATABASE` können auch anders gewählt werden.

### Datenbank einrichten
**Docker Container starten**

    docker compose --env-file .\backend\.env up -d

**Dependencies installieren**

    composer install

**Datenbank migrieren**
    
    docker exec -it praktinizer-backend-1 sh
    php artisan migrate

**Keys erstellen**
    
    php artisan key:generate
    php artisan jwt:secret


## Optional: Dummy Stellenauschreiben hinzufügen (in Container)
    php artisan tinker
    App\Models\Posting::factory()->count(10)->create()