export default {
  name: "Requests",
  registerUser(pEmail, pPassword, pPassword_confirmation) {
    fetch(
      "http://localhost:8000/api/register?" +
        new URLSearchParams({
          email: pEmail,
          password: pPassword,
          password_confirmation: pPassword_confirmation,
        }),
      {
        method: "POST",
      }
    )
      .then((response) => response.json())
      .then((json) => console.log(json));
  },
  loginUser(pEmail, pPassword) {
    const url =
      "http://localhost:8000/api/login?" +
      new URLSearchParams({
        email: pEmail,
        password: pPassword,
      });

    console.log(url);
    fetch(url, {
      method: "POST",
    })
      .then((data) => data.json())
      .then((json) => {
        localStorage.setItem("token", json.token);
        console.log(json);
      });
  },
  /* eslint-disable */
  getPostings() {
    fetch("http://localhost:8000/api/postings", {
      method: "GET",
      withCredentials: true,
      headers: {
        Authorization: "bearer " + localStorage.getItem("token"),
        "Content-Type": "application/json",
      },
    })
      .then((data) => data.json())
      .then((json) => {
        return json;
      });
  },
};
